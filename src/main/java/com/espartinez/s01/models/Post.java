package com.espartinez.s01.models;

import javax.persistence.*;

@Entity
@Table(name="posts")
public class Post {
    // properties
    // id
    @Id
    @GeneratedValue
    private Long id;
    // title
    @Column
    private String title;
    // content
    @Column(length = 5000)
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user; //author
    // constructor
    public Post(){}

    public Post(String title, String content){
        this.title=title;
        this.content=content;
    }
    // Getter $ Setters
    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content=content;
    }
    public User getUser(){
        return user;
    }
    public void setUser(User author) {
        this.user=author;
    }
}
