package com.espartinez.s01.exceptions;

public class UserException extends Exception{
    public UserException(String message){
        super(message);
    }
}
// Exception - not stringboot interface but Java