package com.espartinez.s01.services;

import com.espartinez.s01.config.JwtToken;
import com.espartinez.s01.models.Post;
import com.espartinez.s01.models.User;
import com.espartinez.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService{
    @Autowired
    private UserRepository userRepo;
    @Autowired
    JwtToken jwtToken;

    public void createUser(User newUser){
        userRepo.save(newUser);
    }
    public ResponseEntity updateUser(Long id, User updatedUser, String token){
        User existingUser = userRepo.findById(id).get();
        String authorUsername = existingUser.getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if(authUsername.equals(authorUsername)){
            existingUser.setUsername(updatedUser.getUsername());
            existingUser.setPassword(updatedUser.getPassword());

            userRepo.save(existingUser);
            return new ResponseEntity<>("An existing user was updated", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to update this field", HttpStatus.UNAUTHORIZED);
        }


    }

    public ResponseEntity deleteUser(Long id, String token){
        User existingUser = userRepo.findById(id).get(); //possible because of ORM
        String authorUsername = existingUser.getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if(authUsername.equals(authorUsername)){
            userRepo.deleteById(id);
            return new ResponseEntity<>("User was deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to delete this user", HttpStatus.UNAUTHORIZED);
        }
    }
    public Iterable<User> getUsers(){
        return userRepo.findAll();
    }
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepo.findByUsername(username));
    }
}
