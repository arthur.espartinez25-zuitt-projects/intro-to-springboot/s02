package com.espartinez.s01.services;

import com.espartinez.s01.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    void createUser(User newUser);
    ResponseEntity updateUser(Long id, User updatedUser, String token);
    ResponseEntity deleteUser(Long id, String token);
    Iterable<User> getUsers();
    Optional<User> findByUsername(String username);
}
