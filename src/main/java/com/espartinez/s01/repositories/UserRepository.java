package com.espartinez.s01.repositories;

import com.espartinez.s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {
    User findByUsername(String username);
}
